import board.BoardMain;

import java.util.Arrays;
import java.util.List;

/**
 *
 * Hauptklasse. Argumente für verschiedene Betriebsmodi:
 *
 * -mode=board      Startet im Tafelbetrieb
 * -mode=client     Startet im Clientbetrieb
 * -mode=master     Startet im Masterbetrieb
 *
 * @author David Holzapfel
 */
public class Startup {

    private static final String ARG_BOARD_MODE = "-mode=board";
    private static final String ARG_CLIENT_MODE = "-mode=client";
    private static final String ARG_MASTER_MODE = "-mode=master";

    private static final String ARG_INTERACTIVE_MODE = "-cmd";

    private static final String INVALID_MODE_MESSAGE =
            "Invalid mode in argument list.";

    public static void main(String[] args) {
        List<String> argList = Arrays.asList(args);
        boolean interactiveMode = argList.contains(ARG_INTERACTIVE_MODE);

        //Board-Betrieb
        if(argList.contains(ARG_BOARD_MODE)) {
            new BoardMain(interactiveMode).loop();
        }
        //Client-Betrieb
        else if(argList.contains(ARG_CLIENT_MODE)) {

        }
        //Master-Betrieb
        else if(argList.contains(ARG_MASTER_MODE)) {

        }
        //Kein Betriebsmodus gewählt
        else {
            System.out.println(INVALID_MODE_MESSAGE);
        }
    }

}
