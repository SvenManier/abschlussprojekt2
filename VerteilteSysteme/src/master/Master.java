import java.util.ArrayList;
import java.util.Hashtable;

public class Master 
{
	private static int masterId = 1;

	private String id;
	private String adress;

	private Hashtable<String, String> tables;
	private Hashtable<String, ArrayList<User>> users;
	private ArrayList<Message> backupMessages;

	public Master() 
	{
		tables = new Hashtable<String, String>();		//Tafelid + Adresse
		users = new Hashtable<String, ArrayList<User>>();//Tafelid + liste der zugehörigen user
		backupMessages = new ArrayList<Message>();
		//adresse besetzen

		id = "m0" + masterId;
		masterId++;
	}

	public String getAdress() {
		return adress;
	}
	
	public String getId() {
		return id;
	}

	public Hashtable<String, String> getTables() 
	{
		return tables;
	}

	public Hashtable<String, ArrayList<User>> getUsers() 
	{
		return users;
	}
	
	public void registriereTafel(String id, String adresse)
	{
		tables.put(id, adresse);
		//tafelliste senden
	}
	
	public void entferneTafel(String id)
	{
		if (tables.containsKey(id))
			tables.remove(id);
	}
	
	public void speichereBackupNachricht(Message message)
	{
		backupMessages.add(message);
	}
	
	public ArrayList<Message> sendeBackupNachrichten(String empfaengerId)
	{
		ArrayList<Message> benutzerListe = new ArrayList<Message>();
		for (Message m : backupMessages)
		{
			if (m.getEmpId().equals(empfaengerId))
				benutzerListe.add(m);
		}
		return benutzerListe;
	}
	
	public void entferneBackupNachtichten(String empfaengerId)
	{	
		for (int i = 0; i< backupMessages.size();i++)
		{
			if (backupMessages.get(i).getEmpId().equals(empfaengerId))
				backupMessages.remove(i);
		}		
	}
	
	public void registriereUser(String tafelId, User user)
	{
		if (users.containsKey(tafelId))
		{
			users.get(tafelId).add(user);
		}
		else
			{
				ArrayList<User> tempListe = new ArrayList<User>();
				tempListe.add(user);
				users.put(tafelId, tempListe);
			}
	}

}
