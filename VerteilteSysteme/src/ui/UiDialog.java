package ui;

/**
 * Benutzeroberflaeche fuer das Tafelsystem
 * @author David Kremer
 * @version 1.5
 */


public class UiDialog {
            
       
    private static final int TAFEL_ANMELDEN             =1;
    private static final int BENUTZER_REGISTRIEREN      =2;
    private static final int BENUTZER_ANMELDEN          =3;
    private static final int BENUTZER_LOESCHEN          =4;
    private static final int BENUTZER_ABMELDEN          =5; 
    private static final int TAFEL_LOESCHEN             =6;
    private static final int NACHRICHT_POSTEN           =7;
    private static final int NACHRICHT_BEARBEITEN       =8;
    private static final int NACHRICHT_LOESCHEN         =9;
    private static final int PN_SENDEN                  =10;
    private static final int BEENDEN                    =0;
   
    /**
     * Standart Konstruktor der UI Klasse
     */
    public UiDialog(){
    }
   
  /**
   * Start Methode der UI Klasse
   */
   public void start(){     
       System.out.println("Willkommen im Tafelsystem, was moechten sie tun?");
       System.out.println("Bitte Zahl von 0-10 eingeben!\n");
       int funktion = -1;
                
        while(funktion!= BEENDEN){
            try{
                funktion = einlesenFunktion();
            }catch(Exception e){
                System.out.println (e.getMessage());
                e.printStackTrace();
            }
        }
    }
 /**
  * Main Methode der UI Klasse
  * @param args 
  */
   public static void main (String[] args){
       UiDialog d = new UiDialog();
       d.start();
   }
 
 /**
  * Einlesen der Funktion im Menue
  * @return eingelesene Funktion als Integer  
  */
   private int einlesenFunktion(){
       System.out.print
       ( TAFEL_ANMELDEN             +": Tafel Anmelden\r\n"         +
         BENUTZER_REGISTRIEREN      +": Benutzer registrieren\r\n"  +
         BENUTZER_ANMELDEN          +": Benutzer anmelden\r\n"      +
         BENUTZER_ABMELDEN          +": Benutzer abmelden\r\n"      +
         BENUTZER_LOESCHEN          +": Benutzer loeschen\r\n"      +      
         TAFEL_LOESCHEN             +": Tafel loeschen\r\n"         + 
         NACHRICHT_POSTEN           +": Nachricht posten\r\n"       +      
         NACHRICHT_BEARBEITEN       +": Nachricht bearbeiten\r\n"   +
         NACHRICHT_LOESCHEN         +": Nachricht loeschen\r\n"     +      
         PN_SENDEN                  +": PN senden\r\n"              +      
         BEENDEN                    +": Programm beenden\r\n"       );  
   return Stdin.readlnInt();
   }
   
   /**
    * Ausfuehren der ausgewaehlten Funktion
    * @param funktion die auszufuehrende Funktion als Integer
    */
   private void ausfuehrenFunktion(int funktion){
       String id;
       String adress; 
       String name;
       boolean admin;
       String text;
       String authorID;
       boolean global;
       String newText;
             
    switch (funktion){
        case TAFEL_ANMELDEN:
           id = Stdin.readlnString("Neue Tafel ID eingeben: ");
           adress = Stdin.readlnString("Adresse der Tafel eingeben: ");
           name = Stdin.readlnString("Name der Tafel eingeben: ");
           BoardList.addBoard(id, adress, name);                          
           System.out.println("Tafel Angemeldet!");
           break;
       
        case BENUTZER_REGISTRIEREN:
           id = Stdin.readlnString("Neue Benutzer ID eingeben: ");
           name = Stdin.readlnString("Benutzername eingeben: ");
           admin = Stdin.readlnBoolean("Administrator(true/false)?: ");
           MemberList.addMember(id, name, admin);
           System.out.println("Benutzer erfolgreich registriert!");
           break;    
        
        case BENUTZER_ANMELDEN:
           id = Stdin.readlnString("Benutzer ID eingeben: ");
           Memberlist.loginMember(id);
           System.out.println("Sie sind angemeldet!");
           break;
                               
        case BENUTZER_ABMELDEN:
           id = Stdin.readlnString("Benutzer ID eingeben: ");
           Memberlist.logoutMember(id);
           System.out.println("Sie sind abgemeldet!"); 
           break;
        
        case BENUTZER_LOESCHEN:
           id = Stdin.readlnString("Bitte Benutzer ID eingeben: ");
           Memberlist.removeMember(id);
           System.out.println("Benutzer geloescht!");
           break;
                      
        case TAFEL_LOESCHEN:
           id = Stdin.readlnString("Bitte Tafel ID eingeben: ");
           Boardlist.removeBoard(id);
           System.out.println("Tafel geloescht!");
           break;
           
        case NACHRICHT_POSTEN:
           id = Stdin.readlnString("Nachrichten ID eingeben: ");
           text = Stdin.readlnString("Nachricht: ");
           authorID = Stdin.readlnString("Author ID eingeben: ");
           global = Stdin.readlnBoolean("Global (true/false): ");
           PostList.addPoast(id, text, authorID, global)      
           break;   
           
        case NACHRICHT_BEARBEITEN:
           id = Stdin.readlnString("Nachrichten ID eingeben: ");
           newText = Stdin.readlnString("neue Nachricht: ");
           PostList.editPost(id, newText);           
           break;
        
        case NACHRICHT_LOESCHEN:
           id = Stdin.readlnString("Nachrichten ID eingeben: ");
           PostList.removePost(id);
           System.out.println("Nachricht geloescht!");
           break;   
        
        case PN_SENDEN:
            //ToDo
           break;
        
        case BEENDEN:       
           System.out.println("Das Programm wurde beendet!");
           break;
        
        default:
            System.out.println ("Falsche Eingabe! Bitte Zahl von 0-9 eingeben!");
    }   
  }
}                                                                                
                                           
