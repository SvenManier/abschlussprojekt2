package io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public abstract class ProtokollAdapter {
	private BufferedInputStream inputStream;
	private BufferedOutputStream outputStream;
	protected ServerSocket serverSocket;

	public static final int TIMEOUT_PERIOD_LISTEN = 1000 * 5;
	public static final int TIMEOUT_PERIOD_ACCEPT = 1000;

	// OpCodes
	public static final int OPCODE_CONFIRM_COMMAND = 1;
	public static final int OPCODE_CLIENT_LOGIN = 2;
	public static final int OPCODE_CLIENT_LOGOUT = 3;
	public static final int OPCODE_POST_MESSAGE = 4;
	public static final int OPCODE_PRIVATE_MESSAGE = 5;
	public static final int OPCODE_GLOBAL_MESSAGE = 6;
	public static final int OPCODE_DITRIBUTE_MESSAGE = 7;
	public static final int OPCODE_EDIT_MESSAGE = 8;
	public static final int OPCODE_REDIRECT_GLOBAL_MESSAGE = 9;
	public static final int OPCODE_REDIRECT_PRIVATE_MESSAGE = 10;
	public static final int OPCODE_REDIRECT_EDIT_MESSAGE = 11;
	public static final int OPCODE_ANNOUNCE_TABLE = 12;
	public static final int OPCODE_REGISTER_TABLE = 13;
	public static final int OPCODE_LOGIN_TABLE = 14;
	public static final int OPCODE_LOGOUT_TABLE = 15;
	public static final int OPCODE_BACKUP_MESSAGE = 16;
	public static final int OPCODE_ASK_BACKUP_MESSAGE = 17;
	public static final int OPCODE_SEND_TABLE_LIST = 18;
	public static final int OPCODE_SEND_BACKUP = 19;

	// Subkeys
	public static final String SUBKEY_OPCODE = "opCode";
	public static final String SUBKEY_OPCODE_CONFIRM = "opCodeConfirm";
	public static final String SUBKEY_SUCCESS = "success";
	public static final String SUBKEY_CLIENT_ID_SOURCE = "clientIDSource";
	public static final String SUBKEY_CLIENT_ID_DESTINATION = "clientIDDestination";
	public static final String SUBKEY_CLIENT_IP = "clientIP";
	public static final String SUBKEY_TABLE_ID = "tableID";
	public static final String SUBKEY_TABLE_IP = "tableIP";
	public static final String SUBKEY_MESSAGE = "message";
	public static final String SUBKEY_MESSAGE_ID = "msgID";

	// Laengen
	private static final int CLIENT_ID_SOURCE_LENGTH = 1;
	private static final int MESSAGE_ID_LENGTH = 8;
	private static final int LOGIN_IP_LENGTH = 4;
	private static final int MESSAGE_LENGTH = 2;
	
	/**
	 * Alle
	 * @param opCode
	 * @param success Muss nicht verwendet werden, in dem Fall einfach irgendwas reinschreiben
	 */
	public void sendComandConfirm(int opCode, boolean success) {

	}

	/**
	 * Wartet auf eingehende Verbindungen
	 * @throws IOException Wenn innerhalb der von TIMEMOUT_PERIOD_ACCEPT angegebenen Zeit keine Verbindung entsteht
	 */
	public void listen() throws IOException {
		serverSocket.setSoTimeout(TIMEOUT_PERIOD_ACCEPT);
		Socket socket = serverSocket.accept();
		inputStream = new BufferedInputStream(socket.getInputStream());
		outputStream = new BufferedOutputStream(socket.getOutputStream());
	}
	
	/**
	 * Schliesst die Verbindung mit dem anderen Teilnehmer. Sollte nach dem senden/empfangen der sendCommandConfirm()-Methode geschehen
	 * @throws IOException 
	 */
	public void closeConnection() throws IOException{
		serverSocket.close();
	}

	/**
	 * Nimmt Informationen entgegen
	 * @throws IOException Bei unbekanntem OpCode
	 */
	public HashMap<String, String> receive() throws IOException {
		waitForData(1);
		int opCode = inputStream.read();

		// HashMap anlegen
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(SUBKEY_OPCODE, new Integer(opCode).toString());

		// Protokoll vervollstaendigen
		switch (opCode) {
		case OPCODE_CONFIRM_COMMAND:
			recieveConfirmCommand(map);
			break;
		case OPCODE_CLIENT_LOGIN:
			clientLogin(map);
			break;
		case OPCODE_CLIENT_LOGOUT:
			clientLogout(map);
			break;
		case OPCODE_POST_MESSAGE:
			postMessage(map);
			break;
		default:
			throw new IOException("Unknown command: " + opCode);
		}

		return map;
	}

	// #######################################
	// ##### Protokoll-Vervollstaendiger #####
	// #######################################

	private void postMessage(HashMap<String, String> map) throws IOException {
		String clientIDSource = extractString(CLIENT_ID_SOURCE_LENGTH);
		map.put(SUBKEY_CLIENT_ID_SOURCE, clientIDSource);

		String message = extractString(MESSAGE_LENGTH);
		map.put(SUBKEY_MESSAGE, message);

		waitForData(MESSAGE_ID_LENGTH);
		byte[] msgID_bytes = new byte[MESSAGE_ID_LENGTH];
		inputStream.read(msgID_bytes);
		map.put(SUBKEY_MESSAGE_ID, new String(msgID_bytes));
	}

	/**
	 * Methode vervollstaendigt das Protokoll
	 * @param map
	 * @throws IOException
	 */
	private void clientLogout(HashMap<String, String> map) throws IOException {
		String clientIDSource = extractString(CLIENT_ID_SOURCE_LENGTH);
		map.put(SUBKEY_CLIENT_ID_SOURCE, clientIDSource);
	}

	/**
	 * Methode vervollstaendigt das Protokoll
	 * @param map
	 * @throws IOException 
	 */
	private void clientLogin(HashMap<String, String> map) throws IOException {
		String clientIDSource = extractString(CLIENT_ID_SOURCE_LENGTH);
		map.put(SUBKEY_CLIENT_ID_SOURCE, clientIDSource);

		waitForData(LOGIN_IP_LENGTH);
		byte[] loginIP = new byte[4];
		inputStream.read(loginIP);
		String ipString = loginIP[0] + "." + loginIP[1] + "." + loginIP[2] + "." + loginIP[3];
		map.put(SUBKEY_CLIENT_IP, ipString);
	}

	/**
	 * Methode vervollstaendigt das Protokoll
	 * @param map
	 * @throws IOException 
	 */
	private void recieveConfirmCommand(HashMap<String, String> map) throws IOException {
		waitForData(2);

		int dataByte = inputStream.read();
		map.put(SUBKEY_OPCODE_CONFIRM, new Integer(dataByte).toString());

		dataByte = inputStream.read();
		map.put(SUBKEY_SUCCESS, new Integer(dataByte).toString());
	}

	// #######################
	// ##### Subroutinen #####
	// #######################

	private String extractString(int lengthOfNumber) throws IOException {
		waitForData(lengthOfNumber);
		byte[] length_bytes = new byte[lengthOfNumber];
		inputStream.read(length_bytes);

		if (lengthOfNumber > 4)
			throw new IOException("Number too long, can only be <= 4 bytes");

		// Integer aus Bytes zusammenfuegen
		int string_length = 0;
		int currentByte = 0;
		for (int i = 0; i < lengthOfNumber; i++) {
			currentByte = currentByte << (i * 8);
			string_length = string_length | currentByte;
		}

		byte[] string_bytes = new byte[string_length];

		waitForData(string_length);
		inputStream.read(string_bytes);
		return new String(string_bytes);
	}

	/**
	 * Wartet eine bestimmte Zeit darauf, dass n Bytes im Stream vorhanden sind
	 * @throws IOException
	 */
	private void waitForData(int count) throws IOException {
		long timeoutTime = System.currentTimeMillis() + TIMEOUT_PERIOD_LISTEN;
		while (inputStream.available() < count) {
			if (System.currentTimeMillis() > timeoutTime) // Wenn bis zur timeoutTime keine Daten reinkommen, wird abgebrochen
				throw new IOException("Timout reached (" + TIMEOUT_PERIOD_LISTEN + "ms)");
		}
	}
}
