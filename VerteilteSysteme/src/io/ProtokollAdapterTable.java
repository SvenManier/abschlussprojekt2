package io;

import java.io.IOException;
import java.net.ServerSocket;

public class ProtokollAdapterTable extends ProtokollAdapter{
	private String tableID;
	private String masterIP;
	
	public ProtokollAdapterTable(String tableID, String masterIP) throws IOException {
		serverSocket = new ServerSocket(2048);
		this.tableID = tableID;
		this.masterIP = masterIP;
	}
	
	/**
	 * Table -> Client
	 * @param clientIDSource
	 * @param clientIDDestination
	 * @param message
	 * @param msgID
	 */
	public void sendDistributeMessage(String clientIDSource, String clientIDDestination, String message, long msgID){
		
	}
	
	/**
	 * Table -> Client
	 * @param clientIDSource
	 * @param newMessage
	 * @param msgID
	 */
	public void sendEditMessag(String clientIDSource, String newMessage, long msgID){
		
	}
	
	/**
	 * Table -> Table
	 * @param clientIDSource
	 * @param message
	 * @param msgID
	 */
	public void sendRedirectGlobalMessage(String clientIDSource, String message, long msgID){
		
	}
	
	/**
	 * Table -> Table
	 * @param clientIDSource
	 * @param clientIDDestination
	 * @param message
	 * @param msgID
	 */
	public void sendRedirectPrivateMessage(String clientIDSource, String clientIDDestination, String message, long msgID){
		
	}
	
	/**
	 * Table -> Table
	 * @param clientIDSource
	 * @param newMessage
	 * @param msgID
	 */
	public void sendRedirectEditMessage(String clientIDSource, String newMessage, long msgID){
		
	}
	
	/**
	 * Table -> Table
	 * @param tableIPSource
	 * @param tableIPDestination
	 */
	public void sendAnnounceTable(String tableIPSource, String tableIPDestination){
		
	}

	/**
	 * Table -> Master
	 * @param tableIPSource
	 */
	public void sendRegisterTable(String tableIPSource){
		
	}
	
	/**
	 * Table-> Master
	 */
	public void sendLoginTable(){
		
	}
	
	/**
	 * Table-> Master
	 */
	public void sendLogoutTable(){
		
	}
	
	/**
	 * Table-> Master
	 * @param clientIDSource
	 * @param clientIDDestination
	 * @param message
	 * @param msgID
	 */
	public void sendBackupMessage(String clientIDSource, String clientIDDestination, String message, long msgID){
		
	}
	
	/**
	 * Table-> Master
	 */
	public void sendAskBackupMessage(){
		
	}
}
