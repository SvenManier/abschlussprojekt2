package board;

import board.Datenbank.*;
import board.events.NetworkEventListener;
import board.events.NetworkEventManager;
import board.events.eventclasses.*;

/**
 * @author David Holzapfel
 */
public class NetworkListeners {

    public static void createListeners(Database db) {
        //Neue Nachricht von Client
        NetworkEventManager.getInstance().addEventListener(new NetworkEventListener<MessagePostedEvent>() {
            @Override
            public void handleEvent(MessagePostedEvent event) {
                //Erstelle Post
                db.addPost(event.messageId, event.message, event.clientIdSource, event.global);
                Post newPost = null;
                try {
                    newPost = db.getPostFromID(event.messageId);
                } catch (ElementNotFoundException e) { }

                //Leite Post an Clients weiter
                for(Member member : db.getAllMembers()) {
                    NetworkHelper.sendPostToMember(newPost, member);
                }

                //Globaler Post -> an andere Tafeln weiterleiten
                if(event.global) {
                    for(Board board : db.getAllBoards()) {
                        NetworkHelper.sendPostToBoard(newPost, board);
                    }
                }
            }
        }, MessagePostedEvent.class);

        //Client meldet sich an
        NetworkEventManager.getInstance().addEventListener(new NetworkEventListener<ClientLoginEvent>() {
            @Override
            public void handleEvent(ClientLoginEvent event) {
                //Setze Onlinestatus des Clients auf Online
                try {
                    Member client = db.getMemberFromId(event.clientId);
                    client.setOnline(true);
                } catch(ElementNotFoundException e) {
                    //TODO Fehlerbehandlung: Client nicht registriert
                }
            }
        }, ClientLoginEvent.class);

        //Client meldet sich ab
        NetworkEventManager.getInstance().addEventListener(new NetworkEventListener<ClientLogoutEvent>() {
            @Override
            public void handleEvent(ClientLogoutEvent event) {
                //Setze Onlinestatus des Clients auf Offline
                try {
                    Member client = db.getMemberFromId(event.clientId);
                    client.setOnline(false);
                } catch(ElementNotFoundException e) {
                    //TODO Fehlerbehandlung: Client nicht registriert
                }
            }
        }, ClientLogoutEvent.class);

        //Sende PN an Client
        NetworkEventManager.getInstance().addEventListener(new NetworkEventListener<PrivateMessageEvent>() {
            @Override
            public void handleEvent(PrivateMessageEvent event) {
                db.savePM(event.pmId, event.message, event.senderId, event.receiverId);
                try {
                    Member receiver = db.getMemberFromId(event.receiverId);
                    NetworkHelper.sendPMToMember(db.getPMObject(event.pmId), receiver);
                } catch(ElementNotFoundException e) {
                    try {
                        NetworkHelper.sendPMToExternalMember(db.getPMObject(event.pmId));
                    } catch (ElementNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }, PrivateMessageEvent.class);

        //Bearbeite/lösche Post
        NetworkEventManager.getInstance().addEventListener(new NetworkEventListener<PostEditEvent>() {
            @Override
            public void handleEvent(PostEditEvent event) {
                if(event.deletePost) {
                    try {
                        db.removePost(event.postId);
                    } catch (ElementNotFoundException e) {
                        //TODO Fehlerbehandlung: Post gelöscht der nicht existiert
                    }
                } else {
                    try {
                        db.editPost(event.postId, event.newText);
                    } catch (ElementNotFoundException e) {
                        //TODO Fehlerbehandlung: Post bearbeitet der nicht existiert
                    }
                }
            }
        }, PostEditEvent.class);

    }

}


