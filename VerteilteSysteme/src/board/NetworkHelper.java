package board;

import board.Datenbank.Board;
import board.Datenbank.Member;
import board.Datenbank.Post;
import board.Datenbank.PrivateMessage;

/**
 * Helferklasse, um den Netzwerkkram aus den Listenern herauszuabstrahieren.
 *
 * @author David Holzapfel
 */
public class NetworkHelper {

    public static void sendPostToMember(Post post, Member destination) {

    }

    public static void sendPMToMember(PrivateMessage msg, Member destination) {

    }

    public static void sendPMToExternalMember(PrivateMessage msg) {

    }

    public static void sendPostToBoard(Post post, Board destination) {

    }

}
