package board;

import board.Datenbank.Database;
import board.events.NetworkEventListener;
import board.events.NetworkEventManager;
import board.events.eventclasses.MessagePostedEvent;
import board.events.eventclasses.NetworkEvent;
import sun.plugin2.message.Message;

import java.util.Scanner;

/**
 * Enthält Hauptschleife des Programms für den Board-Betrieb.
 *
 * @author David Holzapfel
 */
public class BoardMain {

    private boolean endProgram = false;
    private boolean consoleMode = false;
    private BoardDebugConsole console;
    private Scanner scanner;

    private Database db;

    public BoardMain(boolean consoleMode) {
        if(consoleMode) {
            console = new BoardDebugConsole();
            scanner = new Scanner(System.in);
            this.consoleMode = true;
        }
        this.db = new Database();
        NetworkListeners.createListeners(db);
    }

    public void loop() {
        //Hauptschleife des Programms in der der Eventmanager ge-updatet wird
        while(!endProgram) {
            NetworkEventManager.getInstance().update();

            if(consoleMode && scanner.hasNextLine()) {
                console.handleLine(scanner.nextLine());
            }
        }
    }

}