package board.events.eventclasses;

public class ClientLogoutEvent extends NetworkEvent {

    public String clientId;

    public ClientLogoutEvent(String clientId) {
        this.clientId = clientId;
    }

}
