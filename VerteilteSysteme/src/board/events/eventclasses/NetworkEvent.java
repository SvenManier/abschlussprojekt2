package board.events.eventclasses;

/**
 * Abstrakte Eventklasse, für jedes Netzwerkevent gibt es eine Unterklasse und
 * einen entsprechenden Event-Handler.
 *
 * @author David Holzapfel
 */
public abstract class NetworkEvent { }
