package board.events.eventclasses;

public class ClientLoginEvent extends NetworkEvent {

    public String clientId;

    public ClientLoginEvent(String clientId) {
        this.clientId = clientId;
    }

}
