package board.events.eventclasses;

public class MessagePostedEvent extends NetworkEvent {

    public String clientIdSource;
    public String messageId;
    public String message;
    public boolean global;

    public MessagePostedEvent(String clientIdSource, String messageId, String message, boolean global) {
        this.clientIdSource = clientIdSource;
        this.message = message;
        this.messageId = messageId;
        this.global = global;
    }

}
