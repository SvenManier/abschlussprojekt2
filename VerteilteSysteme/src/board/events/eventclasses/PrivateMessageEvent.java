package board.events.eventclasses;

import board.Datenbank.PrivateMessage;

public class PrivateMessageEvent extends NetworkEvent {

    public String pmId;
    public String senderId;
    public String receiverId;
    public String message;

    public PrivateMessageEvent(String pmId, String senderId, String receiverId, String message) {
        this.pmId = pmId;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.message = message;
    }

}
