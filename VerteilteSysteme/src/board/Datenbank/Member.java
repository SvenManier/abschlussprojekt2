package board.Datenbank;

/**
 * 
 * @author -Philip-
 * Klasse zum Speichern eines Nutzers
 * 23.03.2016
 */

public class Member{
	private String id;
	private String name;
	private boolean online;
	private boolean admin;
	
	public Member (String id, String name, boolean admin){
		this.id = id;
		this.name = name;
		this.online = false;
		this.admin = admin;
	}

	public String getID() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public boolean isAdmin(){
		return admin;
	}
	
	public void setAdmin(boolean bool){
		this.admin = bool;
	}
	
	public boolean isOnline(){
		return online;
	}
	
	public void setOnline(boolean bool){
		this.online = bool;
	}
}