package board.Datenbank;
/**
 * Exceptionklasse
 * @author -Philip-
 * 23.03.2016
 */
public class ElementNotFoundException extends Exception {
	private static final long serialVersionUID = 8650116767346323485L;

    public ElementNotFoundException(String msg) {
	super(msg);
    }
}
