/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import board.Datenbank.Board;
import board.Datenbank.Database;
import board.Datenbank.Member;
import io.ProtokollAdapter;
import board.Datenbank.Post;
import board.Datenbank.PrivateMessage;
import board.Datenbank.ElementNotFoundException;
import io.ProtokollAdapterClient;
import io.ProtokollAdapter;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Map.*;

/**
 *
 * @author Barry
 */
public class Client extends ProtokollAdapterClient {

    //Attribute
    //private String tableIP;
    //private String clientID;
    private ProtokollAdapter proto;
    private Member member;
    private Database database;
    private Board board;
    private Post post;
    private String clientIP;

    /**
     *
     * @param tableIP
     * @param clientID
     * @param member
     * @param database
     * @param board
     * @param proto
     * @throws IOException
     */
    public Client(String tableIP, String clientID, Member member, Database database, Board board, ProtokollAdapter proto, Post post) throws IOException {

        super(clientID, tableIP);
        serverSocket = new ServerSocket(2048);
        this.board = board;
        this.member = member;
        this.database = database;
        this.post = post;
        this.proto = proto;

    }

    //Constructor
    /**
     * public Client() throws IOException { proto = new ProtokollAdapter; member
     * = new Member(); database = new Database(); }
     *
     */
    /**
     * sendClientLogout
     *
     */
    @Override
    public void sendClientLogout() {
            
        try {

            int opcode= ProtokollAdapter.OPCODE_CLIENT_LOGOUT;
            String clientID;
            clientID = member.getID();
            String tableID;
            tableID = board.getID();
            String tableID_clientID;
            tableID_clientID = new StringBuilder(tableID).append(clientID).toString();
            database.logoutMember(tableID_clientID);

            // here: How to call methode logout from ProtokollAdapter via receive ?
             String getOpcode;
            try {
                getOpcode = proto.receive().get(opcode);
            } catch (IOException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }
                
            System.exit(0);
        } catch (ElementNotFoundException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Login
     *
     *
     * @param loginIP
     *
     */
    @Override
    public void sendClientLogin(String loginIP) {
        try {
            proto.listen();

            try {
                int opcode= proto.OPCODE_LOGIN_TABLE;
                String clientID;
                clientID = database.doesMemberExist(member.getName());
                String tableID;
                tableID = board.getID();
                loginIP = new StringBuilder(tableID).append(clientID).toString();
                database.loginMember(loginIP);

                // Here: how to call Login from ProtokollAdapter ?
                String getOpcode;
                getOpcode = proto.receive().get(opcode);
                
            } catch (ElementNotFoundException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * sendPostMessage Client -> Table
     *
     * @param message
     * @param msgID
     */
    public void sendPostMessage(String message, String msgID) {
        try {
            int opcode= proto.OPCODE_POST_MESSAGE;
            String text =post.getText();
            String tableID= board.getID();
            String ClientID= database.doesMemberExist(member.getName());
            msgID=post.getID();
            String tableID_clientID=new StringBuilder(tableID).append(clientID).toString();
            database.addPost(msgID,text, tableID_clientID, true);
            
            try {
                //Here: how to call send from protokollAdapter,via receive ?

               String getOpcode;
                getOpcode = proto.receive().get(opcode);
                
            } catch (IOException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } catch (ElementNotFoundException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
    
}
